Plugin 'tpope/vim-commentary'
Plugin 'elixir-editors/vim-elixir'
Plugin 'kchmck/vim-coffee-script'
Plugin 'digitaltoad/vim-pug'
Plugin 'groenewege/vim-less'
Plugin 'tkztmk/vim-vala'
Plugin 'dart-lang/dart-vim-plugin' 
Plugin 'Prajjwal/vim-iolang'
Plugin 'clojure-vim/clojure.vim'
